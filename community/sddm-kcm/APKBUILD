# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sddm-kcm
pkgver=5.26.0
pkgrel=0
pkgdesc="Config module for SDDM"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-only"
depends="
	sddm
	systemsettings
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kxmlgui-dev
	libxcursor-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	samurai
	xcb-util-image-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
6b85d951727f9215d0eceb5222dede6d87a104bacdbaf34347cecdad206563620b346e816ae4f710bfd358e7e29570c6a3ad1c15329d51121b94f2d1fe32cac3  sddm-kcm-5.26.0.tar.xz
"
