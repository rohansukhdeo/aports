# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-pastedeploy
pkgver=3.0
pkgrel=0
pkgdesc="Load, configure, and compose WSGI applications and servers"
url="https://pylonsproject.org"
arch="noarch"
license="MIT"
depends="python3 py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-pytest py3-pytest-cov"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/P/PasteDeploy/PasteDeploy-$pkgver.tar.gz"
builddir="$srcdir/PasteDeploy-$pkgver"

build() {
	mkdir -p dist
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m installer -d testenv \
		dist/PasteDeploy-$pkgver-py3-none-any.whl
	local sitedir="$(python3 -c 'import site;print(site.getsitepackages()[0])')"
	PYTHONPATH="$PWD/testenv/$sitedir" python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/PasteDeploy-$pkgver-py3-none-any.whl
}

sha512sums="
bc71db9fbffcbc2093986be9645c6f1905515ba16e55a566b97beb16737527cfc281e63486723890b6dbdda99baea1e6b976e867fdf25d840e769c74504a91da  py3-pastedeploy-3.0.tar.gz
"
