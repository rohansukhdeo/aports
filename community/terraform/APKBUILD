# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.3.2
pkgrel=0
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/refs/tags/v$pkgver.tar.gz"
options="net"

export GOFLAGS="$GOFLAGS -modcacherw -mod=readonly -trimpath -buildvcs=false"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o bin/$pkgname \
		-ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease="
}

check() {
	case "$CARCH" in
	arm*|x86)
		go list . | xargs -t -n4 \
			go test -timeout=2m -parallel=4
		;;
	*)
		go test ./...
		;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
75afef81e4c1444a30e43459d6d6880127f7128898a399c0cc1ce286d1f79b459b5da1dd9cd286c4e27cb5b3e313ab591161bcafa92a31bdbb178ba9c863c7fa  terraform-1.3.2.tar.gz
"
